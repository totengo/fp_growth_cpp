#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include<iterator>
#include <vector> 

using namespace std;

typedef struct item {
	string titulo;
	int suporte_abs;
} item;

typedef vector<item> conjunto;

typedef struct regra {
	conjunto antecedente;
	conjunto consequente;
	double confianca;
	double lift;
} regra;

typedef struct fpTree {
	struct fpTree* pai;
	vector<struct fpTree*> filhos;
	string titulo;
	conjunto conteudo;
	int suporte;
	vector<conjunto> subconjuntos;
	vector<int> suportes;
} fpTree;

int total_transacoes=0;

fpTree* criaElemento(string titulo, conjunto conteudo, int suporte, fpTree* pai) {
	vector<conjunto> vazios;
	vector<int> vaziosi;
	fpTree* criado=new fpTree();
	criado->pai=pai;
	criado->titulo=titulo;
	copy((conteudo).begin(), (conteudo).end(), back_inserter((criado->conteudo)));
	criado->suporte=suporte;
	if(pai!=NULL) {
		if(pai->suporte!=0) {
			copy((pai->subconjuntos).begin(), (pai->subconjuntos).end(), back_inserter((criado->subconjuntos)));
			criado->subconjuntos.push_back(pai->conteudo);
			copy((pai->suportes).begin(), (pai->suportes).end(), back_inserter((criado->suportes)));
			criado->suportes.push_back(pai->suporte);
		}
		else {
			criado->subconjuntos=vazios;
			criado->suportes=vaziosi;
		}
		(pai->filhos).push_back(criado);
	}
	else {
		criado->subconjuntos=vazios;
		criado->suportes=vaziosi;
	}
	return criado;
}

string removeSpaces(string str)  
{ 
    str.erase(remove(str.begin(), str.end(), ' '), str.end()); 
    return str; 
} 

bool compara_itens(item i1, item i2) {
	return(i1.suporte_abs>i2.suporte_abs);
}

vector<item> ordena_lista(vector<item> lista, vector<item> ordenado) {
	vector<item> ord=lista;
	vector<item>::iterator it1, it2;
	for(it1=ord.begin();it1!=ord.end();it1++) {
		for(it2=ordenado.begin();it2!=ordenado.end();it2++) {
			if((*it1).titulo==(*it2).titulo) {
				(*it1).suporte_abs=(*it2).suporte_abs;
				break;
			}
		}
	}
	sort(ord.begin(), ord.end(), compara_itens);
	return ord;
}

vector<item> string_p_lista(string str) {
	vector<item> lista;
	string buscando=removeSpaces(str);
	int tamanho=buscando.length();
	int ind=buscando.find(",");
	lista.push_back({buscando.substr(0, ind), 1});
	while(ind>0) {
		tamanho-=(ind+1);
		buscando=buscando.substr(ind+1, tamanho);
		ind=buscando.find(",");
		if(ind>0) 
			lista.push_back({buscando.substr(0, ind), 1});
	}
	return lista;
}

vector<item> conta_suporte(string filename) {
	vector<item> temp;
	vector<item> ordenado;
	vector<item>::iterator it1, it2;
	ifstream leArq;
	string linha;
	leArq.open(filename);
	while(leArq) {
		getline(leArq, linha);
		temp=string_p_lista(linha);
		if(!temp.empty()) {
			total_transacoes-=-1;
			for(it1=temp.begin();it1!=temp.end();it1++) {
				for(it2=ordenado.begin();it2!=ordenado.end();it2++) {
					if((*it2).titulo==(*it1).titulo) {
						(*it2).suporte_abs+=1;
						break;
					}
				}
				if(it2==ordenado.end()) {
					ordenado.push_back({(*it1).titulo, 1});
				}
			}
			temp.clear();
		}
	}
	sort(ordenado.begin(), ordenado.end(), compara_itens);
	return ordenado;
}

void printa_conjunto(conjunto lista) {
	for(int i=0;i<lista.size();++i) 
		cout<<"titulo: "<<lista[i].titulo<<" quantidade: "<<lista[i].suporte_abs<<endl;
}

void obtem_regras(vector<fpTree*> frequentes, double confmin, double total, vector<regra>& regras) {
	int supa, supc;
	double conf;
	vector<fpTree*>::iterator it;
	for(it=frequentes.begin();it!=frequentes.end();it++) {
		supc=(*it)->suporte;
		for(int i=0;i<(*it)->subconjuntos.size();++i) {
			supa=(*it)->suportes[i];
			conf=((double)supc)/((double)supa);
			if(conf>=confmin) {
				regras.push_back({(*it)->subconjuntos[i], (*it)->conteudo, conf, conf/((double)supc)});
			}
		}
	}
}

void conjuntos_frequentes(fpTree* raiz, double supmin, double total, vector<fpTree*>& frequentes) {
	double suprelativo;
	for(int i=0;i<(raiz->filhos).size();i++) {
		suprelativo=(double)(((raiz->filhos)[i])->suporte)/total;
		if(suprelativo>=supmin) {
			frequentes.push_back((raiz->filhos)[i]);
			conjuntos_frequentes((raiz->filhos)[i], supmin, total, frequentes);
		}
	}
}

void cria_FPTREE(fpTree* raiz, conjunto ordenados, string filename) {
	vector<item> temp;
	vector<item> ordenado=ordenados;
	vector<item>::iterator it1, it2;
	ifstream leArq;
	string linha;

	conjunto conteudo;
	fpTree* atual;
	bool efilho;

	leArq.open(filename);
	while(leArq) {
		getline(leArq, linha);
		temp=string_p_lista(linha);
		temp=ordena_lista(temp, ordenado);
		conteudo.clear();
		atual=raiz;
		printa_conjunto(temp);
		for(it1=temp.begin();it1!=temp.end();it1++) {
			conteudo.push_back(*it1);
			efilho=false;
			cout<<"elemento a analisar: "<<(*it1).titulo<<endl;
			cout<<"num. filhos: "<<atual->filhos.size()<<endl;
			cout<<"nome no: "<<atual->titulo<<endl;
			for(int i=0;i<atual->filhos.size();i++) {
				if(((atual->filhos)[i])->titulo==(*it1).titulo) {
					atual=(atual->filhos)[i];
					atual->suporte+=1;
					efilho=true;
					break;
				}
			}
			if(!efilho) {
				atual=criaElemento((*it1).titulo, conteudo, 1, atual);
			}
		}
	}
}

void printa_regra(regra r) {
	cout<<"{";
	for(int i=0;i<r.antecedente.size();++i) {
		cout<<r.antecedente[i].titulo<<",";
	}
	cout<<"} -> {";
	for(int i=0;i<r.consequente.size();++i) {
		cout<<r.consequente[i].titulo<<",";
	}
	cout<<"}"<<endl;
}

int main(int argc, char** argv) {
	string nome(argv[1]);
	vector<item> ordenado=conta_suporte(nome);

	//teste dos conjuntos
	printa_conjunto(ordenado);
	cout<<"total de transacoes: "<<total_transacoes<<endl;
	ifstream leArq;
	string linha;
	leArq.open(argv[1]);
	getline(leArq, linha);
	getline(leArq, linha);
	getline(leArq, linha);
	conjunto temp=string_p_lista(linha);
	temp=ordena_lista(temp, ordenado);
	printa_conjunto(temp);

	//cria a FP-Tree
	conjunto vazio;
	cout<<"crio a raiz9"<<endl;
	fpTree* raiz=criaElemento("", vazio, 0, NULL);	
	cout<<(*raiz).titulo<<endl;
	cria_FPTREE(raiz, ordenado, nome);
	vector<fpTree*> frequentes;
	conjuntos_frequentes(raiz, 0.01, total_transacoes, frequentes);
	cout<<"num. conjuntos frequentes: "<<frequentes.size()<<endl;
	/*for(int i=0;i<frequentes.size();++i) {
		cout<<"conjunto: "<<i+1<<" freq.: "<<frequentes[i]->suporte<<endl;
		printa_conjunto(frequentes[i]->conteudo);
	}*/
	vector<regra> regras;
	obtem_regras(frequentes, 0.001, total_transacoes, regras);
	cout<<"num. regras obtidas: "<<regras.size()<<endl;
	for(int i=0;i<regras.size();++i) {
		printa_regra(regras[i]);
	}
}